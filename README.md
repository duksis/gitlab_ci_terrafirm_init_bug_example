# GitLab Terraform GCS backend example

Repo to reproduce Terraform GCS backend bug on GitLab CI infrastructure

## Setup

set `GOOGLE_CREDENTIALS`

## Run locally

```
  $ gitlab-runner exec docker validate \
    --docker-volumes "$HOME/.ssh/id_rsa:/root/.ssh/id_rsa:ro" \
    --env "GOOGLE_CREDENTIALS=$GOOGLE_CREDENTIALS"

```
